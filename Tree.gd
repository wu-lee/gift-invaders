extends Node2D

signal fired


# Called when the node enters the scene tree for the first time.
func _ready():
	print_debug("tree")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("ui_right"):
		move_local_x(10)
	
	if Input.is_action_pressed("ui_left"):
		move_local_x(-10)

	if Input.is_action_pressed("ui_down"):
		move_local_y(10)
	
	if Input.is_action_pressed("ui_up"):
		move_local_y(-10)
	
	if Input.is_action_pressed("ui_accept"):
		fire()

func fire():
	if $FiringDelay.is_stopped():
		$FiringDelay.start()
		emit_signal("fired", position + $FirePosition2D.position)


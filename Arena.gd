extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var star_scene = load("res://Star.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	print_debug("arena")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func spawn_star(pos):
	var star_instance = star_scene.instance()
	star_instance.set_name("star")
	star_instance.position = pos
	add_child(star_instance)
	

func _on_TreeNode_fired(pos):
	spawn_star(pos)
